import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;

import static org.mockito.Mockito.*;

public class BirthdayServiceTest {

    private GreetingsService greetingsService = mock(GreetingsService.class);
    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    @Inject private BirthdayService birthdayService;

    @BeforeEach
    public void setUpBirthdayService() {
        Guice.createInjector(new AbstractModule() {

            @Override
            protected void configure() {
                bind(GreetingsService.class).toInstance(greetingsService);
                bind(EmployeeRepository.class).toInstance(employeeRepository);
            }
        }).injectMembers(this);
    }

    @BeforeEach
    public void setUpEmployeeRepository() {
        when(employeeRepository.findEmployeesBornOn(30,1))
                .thenReturn(Arrays.asList(new Employee("Biglia", "Lucas", LocalDate.of(1986, 1, 30), "biglia@acmilan.com")));
    }

    @Test
    public void testSendGreetings() throws Exception {
        birthdayService.sendGreetings(30, 1);
        verify(greetingsService).sendTo("biglia@acmilan.com", "Happy birthday, dear Lucas!");
    }

    @Test
    public void testNoSendGreetings() throws Exception {
        birthdayService.sendGreetings(14, 7);
        verifyZeroInteractions(greetingsService);
    }
}
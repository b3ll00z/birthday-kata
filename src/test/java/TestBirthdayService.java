import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class TestBirthdayService {


    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Mock EmployeeRepository er = mock(EmployeeRepository.class);
    @Mock GreetingsService gs = mock(GreetingsService.class);

    @InjectMocks BirthdayService bds;

    @Test
    public void noneGreetings() {

        when(er.findEmployeesBornOn(anyInt(), anyInt())).thenReturn(Collections.emptyList());
        bds.sendGreetings(anyInt(), anyInt());
        verify(gs, never()).sendTo(anyString(), anyString());
    }

    @Test
    public void foundOneGreetings() {

        Employee e = mock(Employee.class);
        when(er.findEmployeesBornOn(anyInt(), anyInt())).thenReturn(Arrays.asList(e));
        when(e.getEmail()).thenReturn("joe@foo.com");
        when(e.getFirstName()).thenReturn("Joe");

        bds.sendGreetings(anyInt(), anyInt());
        verify(gs, atLeastOnce()).sendTo(eq("joe@foo.com"), eq("Happy birthday, dear Joe!"));
    }

    @Test
    public void foundMoreGreetings() {

        Employee e1 = mock(Employee.class);
        Employee e2 = mock(Employee.class);

        when(er.findEmployeesBornOn(anyInt(), anyInt())).thenReturn(Arrays.asList(e1, e2));
        when(e1.getEmail()).thenReturn("mark@barnesandnoble.com");
        when(e1.getFirstName()).thenReturn("Mark");
        when(e2.getEmail()).thenReturn("john@cdc.gov");
        when(e2.getFirstName()).thenReturn("John");

        bds.sendGreetings(anyInt(), anyInt());
        verify(gs, times(2)).sendTo(any(), any());
        verify(gs, times(1)).sendTo(eq("mark@barnesandnoble.com"), eq("Happy birthday, dear Mark!"));
        verify(gs, times(1)).sendTo(eq("john@cdc.gov"), eq("Happy birthday, dear John!"));
    }
}

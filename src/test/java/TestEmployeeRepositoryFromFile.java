import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.tuple;

public class TestEmployeeRepositoryFromFile {

    private static final String filename = "employees.txt";
    private static String header = "last_name, first_name, date_of_birth, email\n";
    private static String payload =
            "Norcott, Cleo, 10/07/1985, cnorcott0@yellowbook.com\n" +
            "Bullier, Lezley, 1/04/1990, lbullier1@skyrock.com\n" +
            "McNab, Marshall, 7/11/1995, mmcnab2@apple.com\n" +
            "Petrasek, Clem, 7/11/1982, cpetrasek3@fc2.com\n" +
            "Sweeny, Ginni, 11/01/1991, gsweeny4@cpanel.net\n";

    private void setUpFile(String content) throws FileNotFoundException {

        PrintWriter pw = new PrintWriter(filename);
        pw.write(content);
        pw.close();
    }

    @Test
    public void testFileNotFound() throws FileNotFoundException {

        assertThatThrownBy(() -> new EmployeeRepositoryFromFile(new File("foo.txt")))
                        .isInstanceOf(FileNotFoundException.class)
                        .withFailMessage("foo.txt not found!");
    }

    @Test
    public void testMissedHeaderException() throws FileNotFoundException {

        setUpFile(payload);
        assertThatThrownBy(() -> new EmployeeRepositoryFromFile(new File(filename)))
                        .isInstanceOf(InputMismatchException.class)
                        .withFailMessage("Missed header!");
    }

    @Test
    public void testBirthdayNotFound() throws FileNotFoundException {

        setUpFile(header + payload);
        EmployeeRepository er = new EmployeeRepositoryFromFile(new File(filename));

        assertThat(er.findEmployeesBornOn(1, 1)).isEmpty();
    }

    @Test
    public void testBirthdayOnEmployeesFile() throws FileNotFoundException {

        setUpFile(header + payload);
        EmployeeRepository er = new EmployeeRepositoryFromFile(new File(filename));

        assertThat(er.findEmployeesBornOn(7, 11)).asList().hasSize(2);
    }

    @Test
    public void testBirthdayOnNico() throws FileNotFoundException {

        setUpFile(header + payload + "Senten, Nico, 18/10/1986, ns@walmart.com\n");
        Employee nico = new Employee("Senten", "Nico", LocalDate.of(1986, 10, 18), "ns@walmart.com");
        EmployeeRepository er = new EmployeeRepositoryFromFile(new File(filename));
        List<Employee> filteredEmployees = er.findEmployeesBornOn(18, 10);

        assertThat(filteredEmployees)
                .extracting("firstName", "lastName", "birthday", "email")
                .contains(tuple(nico.getFirstName(), nico.getLastName(), nico.getBirthday(), nico.getEmail()));
    }
}

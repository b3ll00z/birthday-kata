public interface GreetingsService {

    void sendTo(String email, String content);
}

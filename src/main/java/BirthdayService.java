import com.google.inject.Inject;

import java.util.List;

public class BirthdayService {

    private EmployeeRepository employeeRepository;
    private GreetingsService greetingsService;

    @Inject
    public BirthdayService(EmployeeRepository er, GreetingsService gs) {
        employeeRepository = er;
        greetingsService = gs;
    }

    public void sendGreetings(Integer day, Integer month) {

        List<Employee> greetingsList = employeeRepository.findEmployeesBornOn(day, month);

        for (Employee employee : greetingsList)
            greetingsService.sendTo(employee.getEmail(), "Happy birthday, dear " + employee.getFirstName() + "!");
    }
}

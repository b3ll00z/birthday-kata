
public class EmailGreetingsService implements GreetingsService {

    @Override
    public void sendTo(String email, String content) {

        System.out.println("To: " + email);
        System.out.println("Subject: Happy birthday!");
        System.out.println(content);
        System.out.println();
    }
}

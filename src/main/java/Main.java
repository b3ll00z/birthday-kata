import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;


public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        URL url = Thread.currentThread().getContextClassLoader().getResource("employees.txt");
        String filename = url.getPath();

        BirthdayService birthdayService = new BirthdayService(
                new EmployeeRepositoryFromFile(new File(filename)), new EmailGreetingsService());

        birthdayService.sendGreetings(7, 11);
    }
}

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class EmployeeRepositoryFromFile implements EmployeeRepository {

    private List<Employee> employees;

    public EmployeeRepositoryFromFile(File employeesFile) throws FileNotFoundException {

        if (!employeesFile.exists())
            throw new FileNotFoundException(employeesFile.getName() + " not found!");

        employees = new ArrayList<Employee>();
        setEmployees(employeesFile);

    }

    private void setEmployees(File employeesFile) throws FileNotFoundException {

        Scanner in = new Scanner(employeesFile);

        StringTokenizer stk;
        String header = in.nextLine();
        String header1 = null;
        String header2 = null;
        String header3 = null;
        String header4 = null;
        stk = new StringTokenizer(header, ", ");

        String employeeLine;
        String lastName = null;
        String firstName = null;
        LocalDate birthday = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        String email = null;

        while(stk.hasMoreTokens()) {

            header1 = stk.nextToken();
            header2 = stk.nextToken();
            header3 = stk.nextToken();
            header4 = stk.nextToken();
        }

        if (!header1.equals("last_name") ||
                !header2.equals("first_name") ||
                    !header3.equals("date_of_birth") ||
                        !header4.equals("email"))
            throw new InputMismatchException("Missed header!");

        while (in.hasNextLine()) {

            employeeLine = in.nextLine();
            stk = new StringTokenizer(employeeLine, ", ");

            while (stk.hasMoreTokens()) {
                lastName  = stk.nextToken();
                firstName = stk.nextToken();
                birthday  = LocalDate.parse(stk.nextToken(), formatter);
                email     = stk.nextToken();
            }

            employees.add(new Employee(lastName, firstName, birthday, email));
        }
    }

    @Override
    public List<Employee> findEmployeesBornOn(Integer day, Integer month) {

        return employees
                .stream()
                .filter(employee -> employee.isMyBirthday(day, month))
                .collect(Collectors.toList());
    }
}

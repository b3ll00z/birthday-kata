import java.util.List;

public interface EmployeeRepository {

    List<Employee> findEmployeesBornOn(Integer day, Integer month);
}
